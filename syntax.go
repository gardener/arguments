package arguments

import (
	"strings"
	"unicode"
)

var endQuotes endQuote

func init() {
	// Create a list of all the end quotes
	for _, r := range QuotePairs {
		endQuotes += r
	}
}

var QuotePairs = map[string]endQuote{
	// Normal quotes
	"'":  "'",
	"\"": "\"",
	// Smart quotes
	"\u201C\u201D\u201F\u201E": "\u201C\u201D\u201F",
	"\u2018\u2019\u201B\u201A": "\u2018\u2019\u201B",
	// Chevrons
	"\u00AB\u300A": "\u00BB\u300B",
	"\u00BB\u300B": "\u00AB\u300A",
	"\u2039\u3008": "\u203A\u3009",
	"\u203A\u3009": "\u2039\u3008",
	// Corner brackets
	"\u300C\u300E": "\u300D\u300F",
}

type endQuote string

func (e endQuote) Is(r rune) bool { return isEndquote(r, string(e)) }

// isQuote checks if r is a starting quote, if so it returns the end quotes and true
func isQuote(r rune) (endQuote, bool) {
	for opening, closing := range QuotePairs {
		if strings.IndexRune(opening, r) != -1 {
			return closing, true
		}
	}
	return "", false
}

// isEndquote checks if r is one of the given ending quotes
// (or really anything in any string but its supposed to be for ending quotes)
func isEndquote(r rune, endQuotes string) bool {
	return strings.IndexRune(endQuotes, r) != -1
}

func isSyntax(r rune) bool {
	return unicode.IsSpace(r) || strings.IndexRune(syntaxChars, r) != -1
}

func isEscape(r rune) bool { return r == '\\' }

var syntaxChars = "\\"

func init() {
	for open, close := range QuotePairs {
		// Add all opening quotes to the list of syntax characters
		for _, r := range open {
			if strings.IndexRune(syntaxChars, r) == -1 {
				syntaxChars += string(r)
			}
		}
		// Add all closing quotes to the list of syntax characters
		for _, r := range close {
			if strings.IndexRune(syntaxChars, r) == -1 {
				syntaxChars += string(r)
			}
		}
	}

	// debug print
	// fmt.Println("syntaxChars:", syntaxChars)
}
