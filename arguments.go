package arguments

// Argument is a single argument
type Argument struct {
	// argument start and end positions
	// be aware that this is the rune positions, not the byte positions
	Start, End int

	Content string

	// if the argument was escaped via being prefixed with \
	Escaped bool
	Quoted  bool
}

// Arguments represents a list of arguments
type Arguments struct {
	Cursor int // Cursor is the current position in the list of arguments

	Original  string
	Arguments []Argument
}

// Len returns the number of arguments
func (a *Arguments) Len() int {
	return len(a.Arguments)
}

// Reset the cursor to the beginning
func (a *Arguments) Reset() {
	a.Cursor = 0
}

// Strings returns the content of each argument as a slice of strings
func (a Arguments) Strings() []string {
	var args []string
	for _, arg := range a.Arguments {
		args = append(args, arg.Content)
	}
	return args
}

// Next returns the next argument
func (a *Arguments) Next() (Argument, bool) {
	if a.Cursor >= len(a.Arguments)-1 {
		return Argument{}, false
	}
	if a.Cursor < 0 {
		// if the cursor is negative, set it to -1 so it returns #0
		a.Cursor = -1
	}
	a.Cursor++
	return a.Arguments[a.Cursor], true
}

// Current returns the current argument
func (a *Arguments) Current() (Argument, bool) {
	if a.Cursor >= len(a.Arguments) || a.Cursor < 0 {
		return Argument{}, false
	}
	return a.Arguments[a.Cursor], true
}

// Previous returns the previous argument
func (a *Arguments) Previous() (Argument, bool) {
	if a.Cursor <= 0 {
		return Argument{}, false
	}
	if a.Cursor > len(a.Arguments) {
		a.Cursor = len(a.Arguments)
	}
	a.Cursor--
	return a.Arguments[a.Cursor], true
}

// Seek moves the cursor to the given index
// if it returns false it means it cannot seek to that position
func (a *Arguments) Seek(index int) bool {
	if index < 0 || index >= len(a.Arguments) {
		return false
	}
	a.Cursor = index
	return true
}

// SeekBy moves the cursor to the given index relative to the current cursor
func (a *Arguments) SeekBy(offset int) bool {
	new := a.Cursor + offset
	if new < 0 || new >= len(a.Arguments) {
		return false
	}
	a.Cursor = new
	return true
}

// Peek returns the next argument without advancing the cursor
func (a *Arguments) Peek() (Argument, bool) {
	return a.PeekBy(1)
}

// PeekBy returns the argument at the given index relative to the current cursor
func (a *Arguments) PeekBy(offset int) (Argument, bool) {
	pos := a.Cursor + offset
	if pos < 0 || pos >= len(a.Arguments) {
		return Argument{}, false
	}
	return a.Arguments[a.Cursor+offset], true
}

// Raw returns the original string between the
// start and end positions of an argument
func (a *Arguments) Raw(index int) (string, bool) {
	if index < 0 || index >= len(a.Arguments) {
		return "", false
	}
	return a.Original[a.Arguments[index].Start:a.Arguments[index].End],
		true
}

// RawBy returns the raw string of an argument at the given index relative to the current cursor
func (a *Arguments) RawBy(offset int) (string, bool) {
	return a.Raw(a.Cursor + offset)
}

// RawRange returns the original string between a range of arguments
func (a *Arguments) RawRange(start, end int) (string, bool) {
	if start < 0 || start >= len(a.Arguments) {
		return "", false
	}
	if end < 0 || end > len(a.Arguments) {
		return "", false
	}
	if end <= start {
		return "", false
	}
	return a.Original[a.Arguments[start].Start:a.Arguments[end-1].End], true
}

// Range returns a copy of a containing only the arguments
// between the start and end positions
func (a *Arguments) Range(start, end int) (Arguments, bool) {
	if start < 0 || start >= len(a.Arguments) {
		return Arguments{}, false
	}
	if end < 0 || end > len(a.Arguments) {
		return Arguments{}, false
	}
	if end <= start {
		return Arguments{}, false
	}

	args := Arguments{
		Original:  a.Original[a.Arguments[start].Start:a.Arguments[end-1].End],
		Arguments: a.Arguments[start:end],
	}

	// reanchor the start and end positions
	start = a.Arguments[start].Start

	for i := range args.Arguments {
		args.Arguments[i].Start -= start
		args.Arguments[i].End -= start
	}

	return args, true
}

// Append combines two argument sets into one, returning the combined list
func (a Arguments) Append(b *Arguments) Arguments {
	a.Original += " " + b.Original

	// reanchor the start and end positions of the second list
	start := a.Arguments[a.Len()-1].End + 1
	for i := range b.Arguments {
		b.Arguments[i].Start += start
		b.Arguments[i].End += start
	}

	a.Arguments = append(a.Arguments, b.Arguments...)

	return a
}

// AppendString parses a string and appends it to the argument list
// returning the combined list
func (a Arguments) AppendString(s string) (Arguments, bool) {
	b, ok := Parse(s)
	if !ok {
		return a, false
	}
	return a.Append(&b), true
}
