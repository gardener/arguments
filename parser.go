package arguments

import (
	"strings"
	"unicode"

	"codeberg.org/gardener/arguments/scanner"
)

// Parse is a function that parses the given string into a list of arguments
// using the following syntax.
// - Spaces are used as delimiters to separate arguments.
//   - Duplicate spaces are ignored.
// - Quotes are used to create a single argument ignoring spaces.
//   - (see `quotePairs` for list of supported quote types)
// - `\` is used to escape quotes and spaces or other escapes.
// in addition, an escape may be used at the beginning of an argument to mark
// it as escaped.
func Parse(s string) (_ Arguments, ok bool) {
	scan := scanner.New(s)

	// if there is nothing to parse, we just don't
	if r, _ := scan.FindFunc(isSyntax); r == scanner.RuneNotFound {
		if len(s) == 0 {
			return
		}
		return Arguments{
			Original:  s,
			Arguments: []Argument{{Start: 0, End: len(s), Content: s}},
		}, true
	}
	// otherwise, we parse the string
	scan.Seek(0) // reset the scanner

	args := Arguments{
		Original:  s,
		Arguments: make([]Argument, 0),
	}

	// parser state
	var buffer = &strings.Builder{}
	var escaped = false
	var quote endQuote

	var previous int = 0

	// argument metadata
	var boundaryStart, boundaryEnd int
	var argEscape = false
	var wasQuoted = false

	// save an argument if one is present and reset the state for the next
	appendEntry := func() {
		if len(buffer.String()) == 0 {
			return
		}

		args.Arguments = append(args.Arguments, Argument{
			Start:   boundaryStart,
			End:     boundaryEnd,
			Content: buffer.String(),
			Escaped: argEscape,
			Quoted:  wasQuoted,
		})

		previous = scan.Pos()

		buffer.Reset()
		escaped = false
		quote = ""
		argEscape = false
		wasQuoted = false
	}

loop:
	for !scan.Empty() {
		previous = scan.Pos()

		r, pos := scan.FindFunc(isSyntax)

		// write skipped non-syntax characters to the buffer
		if pos-previous > 0 {
			buffer.WriteString(string(scan.Runes()[previous:pos]))
		}

		// if there are no remaining syntax characters, we're done
		if r == scanner.RuneNotFound {
			buffer.WriteString(scan.Remainder())
			break loop
		}

		if escaped {
			buffer.WriteRune(r)
			escaped = false
			continue
		}

		switch {
		case unicode.IsSpace(r) && quote == "":
			boundaryEnd = pos
			appendEntry()
			boundaryStart = scan.Pos()

		case unicode.IsSpace(r) && quote != "":
			buffer.WriteRune(r)

		case quote.Is(r):
			boundaryEnd = scan.Pos()
			appendEntry()
			boundaryStart = scan.Pos()
			continue

		// peek 0 instead of peek 1 because FindFunc increments one past
		case isEscape(r) && isSyntax(scan.PeekBy(0)):
			escaped = true
		case isEscape(r) && buffer.Len() == 0:
			argEscape = true
		}

		if end, ok := isQuote(r); ok && quote == "" {
			quote = end
			wasQuoted = true
			continue
		}

		if endQuotes.Is(r) {
			buffer.WriteRune(r)
		}
	}

	// save the last argument
	boundaryEnd = len(scan.Runes())
	appendEntry()

	// fmt.Printf("%+v\n", args)

	return args, true
}
