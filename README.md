# Arguments
A command argument parser with some helpful extras. Supports syntax escapes, multilingual quotation marks, and the ability to mark an argument as escaped. As well as preservation of parsing metadata to allow fetching sections of raw unparsed string between arguments.

Also unofficially known as Greg.

## Syntax
----
- Argument Separation via spaces, a 'space' in this case being any unicode space, including linebreaks.
  - ``one two`` > one, two
  - Repeated spaces, Leading spaces, and Trailing spaces, are all ignored.

- `\` Escaping can cancel out any forms of parsed syntax, including spaces, and other escapes.
  - As an extra that may be useful in certain situations, an argument may be prefixed with an escape (`\`) to add a meta flag marking it as 'escaped', what this means if anything is up to the end application.

- Quotation marks may be used to mark a section of text to be treated as a singular argument ignoring any spaces contained between the start and end quote. Supported quotation marks include:
  - Standard single and double quotes.
  - "Smart" quotes, sometimes seen in office software or used by mobile keyboards in place of standard quotes.
  - Chevrons.
  - Japanese angle quotes.

