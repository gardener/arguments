package scanner

// RuneNotFound is a marker used either by FindFunc to mark it didn't find anything
// or returned when there is no relevant rune to find
const RuneNotFound rune = -1

// New returns a new rune scanner
func New(s string) *Scanner {
	return &Scanner{
		runes: []rune(s),
		pos:   -1,
	}
}

// Scanner reads out a string rune by rune
type Scanner struct {
	runes []rune
	pos   int
}

// NotEmpty returns if r has any more runes
func (r *Scanner) Empty() bool { return !r.HasLengthRemaining(0) }

// HasLengthRemaining returns if r has length more runes remaining
func (r *Scanner) HasLengthRemaining(length int) bool { return len(r.runes) >= r.pos+length }

// Remaining returns how many runes are remaining
func (r *Scanner) Remaining() int { return len(r.runes) - r.pos }

// Remainder returns anything not scanned
func (r *Scanner) Remainder() string { return string(r.runes[r.pos:]) }

// Runes returns the runes
func (r *Scanner) Runes() []rune { return r.runes }

// String returns the string
func (r *Scanner) String() string { return string(r.runes) }

// Pos returns the current position
func (r *Scanner) Pos() int { return r.pos }

// Current returns the current rune
func (r *Scanner) Current() rune { return r.PeekBy(0) }

// Incr increments the position and returns the current rune
// it returns RuneNotFound and doesn't increment if it hits the end
func (r *Scanner) Incr() rune {
	r.pos++
	cr := r.PeekBy(0)
	if cr == RuneNotFound {
		r.pos--
	}
	return cr
}

// Decr is the inverse of Incr, decrements the position and returns the current rune
// it returns RuneNotFound and doesn't decrement if it hits the start
func (r *Scanner) Decr() rune {
	r.pos--
	cr := r.PeekBy(0)
	if cr == RuneNotFound {
		r.pos++
	}
	return cr
}

// Peek returns the rune at pos + i
func (r *Scanner) Peek() rune {
	return r.PeekBy(1)
}

// PeekBy returns the rune i after pos if present
func (r *Scanner) PeekBy(i int) rune {
	if x := r.pos + i; x < 0 || x >= len(r.runes) {
		return RuneNotFound
	}
	return r.runes[r.pos+i]
}

// Seek sets the pos to i
func (r *Scanner) Seek(i int) bool {
	if i < -1 || i >= len(r.runes) {
		return false
	}
	r.pos = i
	return true
}

// SeekBy shifts the pos by i
func (r *Scanner) SeekBy(i int) bool {
	return r.Seek(r.pos + i)
}

// FindFunc works like strings.IndexFunc
// it searches for the next rune that satisfies the condition function
// and sets the pointer to that point + 1 so the next call returns the next rune
// returns RuneNotFound and -1 if it doesn't find anything
// returns the rune and its position of it if it finds one
func (r *Scanner) FindFunc(fn func(r rune) bool) (char rune, at int) {
	start := r.pos
	if start < 0 {
		start = 0
	}
	if start >= len(r.runes) {
		return RuneNotFound, -1
	}
	for i, x := range r.runes[start:] {
		if !fn(x) {
			continue
		}
		r.pos += i

		defer func() { r.pos++ }()
		return x, r.pos
	}

	return RuneNotFound, -1
}
