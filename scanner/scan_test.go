package scanner

import (
	"testing"
	"unicode"

	"github.com/matryer/is"
)

// probably not the most thorough set of tests, but at least it'll catch any big issues

func TestScanner(t *testing.T) {
	is := is.New(t)

	const input = "test string!"
	s := New(input)

	t.Log("Testing New() with input:", input)

	s.Incr() // skip the first rune

	// test Current()
	is.Equal(s.Current(), 't')

	// test Incr
	is.Equal(s.Incr(), 'e')

	// test Decr
	is.Equal(s.Decr(), 't')

	// test Empty with non-empty string
	is.Equal(s.Empty(), false)

	// test Seek
	s.Seek(1)
	is.Equal(s.Current(), 'e')

	s.SeekBy(2)
	is.Equal(s.Current(), 't')

	s.SeekBy(-2)
	is.Equal(s.Current(), 'e')

	// test Remaining
	is.Equal(s.Remaining(), 11)

	// test HasLengthRemaining
	is.Equal(s.HasLengthRemaining(11), true)

	// test Remainder
	s.Seek(5)
	t.Log("set pos to 5")
	is.Equal(s.Remainder(), "string!")

	// test Pos
	is.Equal(s.Pos(), 5)

	// test Peek(0)
	is.Equal(s.PeekBy(0), 's')

	// test Peek(-2)
	is.Equal(s.PeekBy(-2), 't')

	// test Peek(1)
	is.Equal(s.PeekBy(1), 't')

	// set pos to 0
	s.Seek(0)
	t.Log("set pos to 0")

	// test Peek()
	is.Equal(s.Peek(), 'e')

	// test PeekBy(1)
	is.Equal(s.PeekBy(1), 'e')

	// test PeekBy(-1)
	is.Equal(s.PeekBy(-1), RuneNotFound)

	// set pos to end
	s.Seek(len(input) - 1)
	t.Log("set pos to end")

	// test Peek(1)
	is.Equal(s.PeekBy(1), RuneNotFound)

	// set post to 0
	s.Seek(0)
	t.Log("set pos to 0")

	// test FindFunc(unicode.IsSpace)
	{
		r, pos := s.FindFunc(unicode.IsSpace)
		is.Equal(r, ' ')
		is.Equal(pos, 4)
	}

	// test FindFunc(unicode.IsSpace) with no match
	{
		r, pos := s.FindFunc(unicode.IsSpace)
		is.Equal(r, RuneNotFound)
		is.Equal(pos, -1)
	}
}

func TestReturns(t *testing.T) {
	is := is.New(t)

	const input = "test string!"

	s := New(input)

	// s.Runes()
	is.Equal(s.Runes(), []rune(input))

	// s.String()
	is.Equal(s.String(), input)
}

func TestOutOfBounds(t *testing.T) {
	is := is.New(t)

	const input = "test string!"

	s := New(input)

	// s.Peek(len(input)+1)
	is.Equal(s.PeekBy(len(input)+1), RuneNotFound)

	// s.Peek(-len(input)-1)
	is.Equal(s.PeekBy(-len(input)-1), RuneNotFound)

	// s.Incr
	s.Seek(len(input) - 1)
	is.Equal(s.Incr(), RuneNotFound)

	// s.Decr
	s.Seek(0)
	is.Equal(s.Decr(), RuneNotFound)

	// s.Seek
	is.Equal(s.Seek(len(input)+1), false)
	is.Equal(s.Seek(-len(input)-1), false)

	// s.FindFunc
	s.pos = -1
	r, pos := s.FindFunc(unicode.IsSpace)
	is.Equal(r, ' ')
	is.Equal(pos, 3)

	s.pos = len(input)
	r, pos = s.FindFunc(unicode.IsSpace)
	is.Equal(r, RuneNotFound)
	is.Equal(pos, -1)
}
