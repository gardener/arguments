package arguments_test

import (
	"fmt"
	"testing"

	"codeberg.org/gardener/arguments"
	"github.com/matryer/is"
)

// a simple test with 2 arguments and no special syntax
func TestSimple(t *testing.T) {
	is := is.New(t)

	const input = "test string!"

	args, ok := arguments.Parse(input)
	is.True(ok)

	is.Equal(len(args.Arguments), 2)
	is.Equal(args.Arguments[0].Content, "test")
	is.Equal(args.Arguments[1].Content, "string!")

	is.Equal(args.Arguments[0].Start, 0)
	is.Equal(args.Arguments[0].End, 4)
	is.Equal(args.Arguments[1].Start, 5)
	is.Equal(args.Arguments[1].End, 12)
}

// a test involving 3 space separated arguments, 2 of which contained in quotes
// should produce 2 arguments
func TestQuotes(t *testing.T) {
	is := is.New(t)

	const input = `test "cool string"`

	args, ok := arguments.Parse(input)
	is.True(ok)

	is.Equal(len(args.Arguments), 2)
	is.Equal(args.Arguments[0].Content, "test")
	is.Equal(args.Arguments[1].Content, "cool string")
	is.Equal(args.Arguments[0].Quoted, false)

	// test boundaries
	is.Equal(args.Arguments[0].Start, 0)
	is.Equal(args.Arguments[0].End, 4)
	is.Equal(args.Arguments[1].Start, 5)
	is.Equal(args.Arguments[1].End, 18)
}

// a test involving 3 arguments to test argument escaping.
// the first and last arguments are prefixed with an escape
// the middle argument is not escaped
func TestEscapedArguments(t *testing.T) {
	is := is.New(t)

	const input = `\test escaped \string`

	args, ok := arguments.Parse(input)
	is.True(ok)

	is.Equal(len(args.Arguments), 3)

	is.Equal(args.Arguments[0].Content, "test")
	is.Equal(args.Arguments[0].Escaped, true)
	is.Equal(args.Arguments[0].Start, 0)
	is.Equal(args.Arguments[0].End, 5)

	is.Equal(args.Arguments[1].Content, "escaped")
	is.Equal(args.Arguments[1].Escaped, false)
	is.Equal(args.Arguments[1].Start, 6)
	is.Equal(args.Arguments[1].End, 13)

	is.Equal(args.Arguments[2].Content, "string")
	is.Equal(args.Arguments[2].Escaped, true)
	is.Equal(args.Arguments[2].Start, 14)
	is.Equal(args.Arguments[2].End, 21)
}

// test an empty string
func TestEmpty(t *testing.T) {
	is := is.New(t)

	const input = ""

	args, ok := arguments.Parse(input)
	is.Equal(ok, false)

	is.Equal(len(args.Arguments), 0)
}

func TestLeadingSpace(t *testing.T) {
	is := is.New(t)

	const input = " test"

	args, ok := arguments.Parse(input)
	is.True(ok)

	is.Equal(len(args.Arguments), 1)
	is.Equal(args.Arguments[0].Content, "test")

	// test boundaries
	is.Equal(args.Arguments[0].Start, 1)
	is.Equal(args.Arguments[0].End, 5)
}

func TestTrailingSpace(t *testing.T) {
	is := is.New(t)

	const input = "test string "

	args, ok := arguments.Parse(input)
	is.True(ok)

	is.Equal(len(args.Arguments), 2)
	is.Equal(args.Arguments[0].Content, "test")
	is.Equal(args.Arguments[1].Content, "string")

	// test boundaries
	is.Equal(args.Arguments[0].Start, 0)
	is.Equal(args.Arguments[0].End, 4)
	is.Equal(args.Arguments[1].Start, 5)
	is.Equal(args.Arguments[1].End, 11)
}

func TestMultipleSpaces(t *testing.T) {
	is := is.New(t)

	const input = "test  string"

	args, ok := arguments.Parse(input)
	is.True(ok)

	is.Equal(len(args.Arguments), 2)
	is.Equal(args.Arguments[0].Content, "test")
	is.Equal(args.Arguments[1].Content, "string")

	// test boundaries
	is.Equal(args.Arguments[0].Start, 0)
	is.Equal(args.Arguments[0].End, 4)
	is.Equal(args.Arguments[1].Start, 6)
	is.Equal(args.Arguments[1].End, 12)
}

func TestBackToBackQuotes(t *testing.T) {
	is := is.New(t)

	const input = `test "string""string"`

	args, ok := arguments.Parse(input)
	is.True(ok)

	is.Equal(len(args.Arguments), 3)
	is.Equal(args.Arguments[0].Content, "test")
	is.Equal(args.Arguments[1].Content, "string")
	is.Equal(args.Arguments[2].Content, "string")

	// test boundaries
	is.Equal(args.Arguments[0].Start, 0)
	is.Equal(args.Arguments[0].End, 4)
	is.Equal(args.Arguments[1].Start, 5)
	is.Equal(args.Arguments[1].End, 13)
	is.Equal(args.Arguments[2].Start, 13)
	is.Equal(args.Arguments[2].End, 21)
}

// a test with 3 arguments all separated by spaces
// but the second space is escaped, expects 2 arguments
func TestEscapedSpaces(t *testing.T) {
	is := is.New(t)

	const input = `test escaped\ string`

	args, ok := arguments.Parse(input)
	is.True(ok)

	is.Equal(len(args.Arguments), 2)
	is.Equal(args.Arguments[0].Content, "test")
	is.Equal(args.Arguments[1].Content, "escaped string")

	// test boundaries
	is.Equal(args.Arguments[0].Start, 0)
	is.Equal(args.Arguments[0].End, 4)
	is.Equal(args.Arguments[1].Start, 5)
	is.Equal(args.Arguments[1].End, 20)
}

// a test with 3 arguments where the 2nd and 3rd arguments are quoted
// but the quotes are escaped so they are not treated as quotes
// expects 2 arguments
func TestEscapedQuotes(t *testing.T) {
	is := is.New(t)

	const input = `test \"string\"`

	args, ok := arguments.Parse(input)
	is.True(ok)

	is.Equal(len(args.Arguments), 2)
	is.Equal(args.Arguments[0].Content, "test")
	is.Equal(args.Arguments[1].Content, "\"string\"")

	// test boundaries
	is.Equal(args.Arguments[0].Start, 0)
	is.Equal(args.Arguments[0].End, 4)
	is.Equal(args.Arguments[1].Start, 5)
	is.Equal(args.Arguments[1].End, 15)
}

// test that escapes can escape escapes, has 3 arguments and should parse to 2.
// - the first is prefixed by 2 escapes, the first escape should nullify the
//   second and the parsed argument should contain a backslash.
// - the second argument is not escaped, but the space preceeding it is so it
//   should be a part of the first argument, not a new argument, and it is
//   followed by a double escape such the output argument should contain a backslash
// - the third argument has no escapes, so it should be parsed as a new argument
func TestEscapedEscapes(t *testing.T) {
	is := is.New(t)

	const input = `\\test\ escaped\\ string`

	args, ok := arguments.Parse(input)
	is.True(ok)

	is.Equal(len(args.Arguments), 2)
	is.Equal(args.Arguments[0].Content, "\\test escaped\\")
	is.Equal(args.Arguments[1].Content, "string")

	// test boundaries
	is.Equal(args.Arguments[0].Start, 0)
	is.Equal(args.Arguments[0].End, 17)
	is.Equal(args.Arguments[1].Start, 18)
	is.Equal(args.Arguments[1].End, 24)
}

// a test for all combinations of recognised end and beginning quotes
// there are 4 arguments in the template, the middle two having placeholders
// for quotes to be inserted.
// it will cycle through all possible combinations of quotes and check that
// the parser correctly handles all pairs correctly.
//
// if the pair the start quote and end quote belong to match,
//    it will parse the quotes and report 3 arguments.
// if the pair the start quote and end quote do not match,
//    it will not count the end quote and will report 2 arguments.
func TestAllQuotePairs(t *testing.T) {
	is := is.New(t)

	const template = `test %vall quotes%v here`

	testPair := func(match bool, open, close string) {
		input := fmt.Sprintf(template, open, close)

		args, ok := arguments.Parse(input)
		is.True(ok)

		if match {
			is.Equal(len(args.Arguments), 3)
			is.Equal(args.Arguments[0].Content, "test")
			is.Equal(args.Arguments[1].Content, "all quotes")
			is.Equal(args.Arguments[2].Content, "here")

			is.Equal(args.Arguments[0].Quoted, false)
			is.Equal(args.Arguments[1].Quoted, true)
			is.Equal(args.Arguments[2].Quoted, false)

			is.Equal(args.Arguments[0].Start, 0)
			is.Equal(args.Arguments[0].End, 4)
			is.Equal(args.Arguments[1].Start, 5)
			is.Equal(args.Arguments[1].End, 17)
			is.Equal(args.Arguments[2].Start, 18)
			is.Equal(args.Arguments[2].End, 22)
		} else {
			is.Equal(len(args.Arguments), 2)
			is.Equal(args.Arguments[0].Content, "test")
			is.Equal(args.Arguments[1].Content, fmt.Sprintf("all quotes%v here", close))

			is.Equal(args.Arguments[0].Quoted, false)
			is.Equal(args.Arguments[1].Quoted, true)

			is.Equal(args.Arguments[0].Start, 0)
			is.Equal(args.Arguments[0].End, 4)
			is.Equal(args.Arguments[1].Start, 5)
			is.Equal(args.Arguments[1].End, 22)
		}
	}

	for openset := range arguments.QuotePairs {
		for openset2, closeset := range arguments.QuotePairs {
			match := openset2 == openset

			for _, open := range openset {
				for _, close := range closeset {
					testPair(match, string(open), string(close))
				}
			}
		}
	}
}

// a test involving all features supported by the parser
// for a more complete stress test.
func TestComplex(t *testing.T) {
	is := is.New(t)

	const input = `  test    string "quoted string" escaped\ space\ \\\\
	line break \escaped argument 'another quote' \"escaped quote\"`

	args, ok := arguments.Parse(input)
	is.True(ok)

	is.Equal(len(args.Arguments), 11)
	is.Equal(args.Arguments[0].Content, "test")
	is.Equal(args.Arguments[1].Content, "string")
	is.Equal(args.Arguments[2].Content, "quoted string")
	is.Equal(args.Arguments[3].Content, "escaped space \\\\")
	is.Equal(args.Arguments[4].Content, "line")
	is.Equal(args.Arguments[5].Content, "break")
	is.Equal(args.Arguments[6].Content, "escaped")
	is.Equal(args.Arguments[7].Content, "argument")
	is.Equal(args.Arguments[8].Content, "another quote")
	is.Equal(args.Arguments[9].Content, "\"escaped")
	is.Equal(args.Arguments[10].Content, "quote\"")

	is.Equal(args.Arguments[0].Quoted, false)
	is.Equal(args.Arguments[1].Quoted, false)
	is.Equal(args.Arguments[2].Quoted, true)
	is.Equal(args.Arguments[3].Quoted, false)
	is.Equal(args.Arguments[4].Quoted, false)
	is.Equal(args.Arguments[5].Quoted, false)
	is.Equal(args.Arguments[6].Quoted, false)
	is.Equal(args.Arguments[7].Quoted, false)
	is.Equal(args.Arguments[8].Quoted, true)
	is.Equal(args.Arguments[9].Quoted, false)
	is.Equal(args.Arguments[10].Quoted, false)

	is.Equal(args.Arguments[0].Escaped, false)
	is.Equal(args.Arguments[1].Escaped, false)
	is.Equal(args.Arguments[2].Escaped, false)
	is.Equal(args.Arguments[3].Escaped, false)
	is.Equal(args.Arguments[4].Escaped, false)
	is.Equal(args.Arguments[5].Escaped, false)
	is.Equal(args.Arguments[6].Escaped, true)
	is.Equal(args.Arguments[7].Escaped, false)
	is.Equal(args.Arguments[8].Escaped, false)
	is.Equal(args.Arguments[9].Escaped, false)
	is.Equal(args.Arguments[10].Escaped, false)
}
