package arguments_test

import (
	"testing"

	"codeberg.org/gardener/arguments"
	"github.com/matryer/is"
)

// i wrote the tests before adding the okay return values to the method signatures
func noOkArgs(a arguments.Arguments, _ bool) arguments.Arguments { return a }
func noOkArg(a arguments.Argument, _ bool) arguments.Argument    { return a }
func noOkStr(a string, _ bool) string                            { return a }

func TestArguments(t *testing.T) {
	is := is.New(t)

	const input = `this is a test string "quoted string" \and\ an\ escape`

	args, ok := arguments.Parse(input)
	is.Equal(ok, true)

	is.Equal(len(args.Arguments), 7)

	// args.Strings
	is.Equal(args.Strings(), []string{"this", "is", "a", "test", "string",
		"quoted string", "and an escape"})

	// movement
	is.Equal(noOkArg(args.Current()).Content, "this")
	is.Equal(noOkArg(args.Previous()).Content, "")
	is.Equal(noOkArg(args.Next()).Content, "is")
	is.Equal(noOkArg(args.Next()).Content, "a")
	is.Equal(noOkArg(args.Next()).Content, "test")
	is.Equal(noOkArg(args.Next()).Content, "string")
	is.Equal(noOkArg(args.Previous()).Content, "test")
	is.Equal(noOkArg(args.Previous()).Content, "a")

	// seeking
	args.Seek(0)
	is.Equal(noOkArg(args.Current()).Content, "this")
	args.Seek(3)
	is.Equal(noOkArg(args.Current()).Content, "test")
	args.SeekBy(-3)
	is.Equal(noOkArg(args.Current()).Content, "this")
	args.SeekBy(4)
	is.Equal(noOkArg(args.Current()).Content, "string")

	// peek
	is.Equal(noOkArg(args.Peek()).Content, "quoted string")

	// peekby
	is.Equal(noOkArg(args.PeekBy(2)).Content, "and an escape")

	// peekby negative
	is.Equal(noOkArg(args.PeekBy(-2)).Content, "a")

	// reset
	args.Reset()
	is.Equal(noOkArg(args.Current()).Content, "this")

	// raw(6)
	is.Equal(noOkStr(args.Raw(5)), `"quoted string"`)

	// raw(7)
	is.Equal(noOkStr(args.Raw(6)), `\and\ an\ escape`)

	// raw(8)
	is.Equal(noOkStr(args.Raw(7)), ``)

	// rawrange(2, 6)
	is.Equal(noOkStr(args.RawRange(2, 6)), `a test string "quoted string"`)

	// rawrange(2, 7)
	is.Equal(noOkStr(args.RawRange(2, 7)),
		`a test string "quoted string" \and\ an\ escape`)

	// range(3, 7)
	subargs := noOkArgs(args.Range(4, 7))
	// fmt.Printf("%#v\n", subargs)
	is.Equal(len(subargs.Arguments), 3)
	is.Equal(subargs.Arguments[0].Content, "string")
	is.Equal(subargs.Arguments[1].Content, "quoted string")
	is.Equal(subargs.Arguments[2].Content, "and an escape")

	// subargs arguments quoted
	is.Equal(subargs.Arguments[0].Quoted, false)
	is.Equal(subargs.Arguments[1].Quoted, true)
	is.Equal(subargs.Arguments[2].Quoted, false)

	// subargs arguments escaped
	is.Equal(subargs.Arguments[0].Escaped, false)
	is.Equal(subargs.Arguments[1].Escaped, false)
	is.Equal(subargs.Arguments[2].Escaped, true)

	// subargs boundaries
	is.Equal(subargs.Arguments[0].Start, 0)
	is.Equal(subargs.Arguments[0].End, 6)
	is.Equal(subargs.Arguments[1].Start, 7)
	is.Equal(subargs.Arguments[1].End, 22)
	is.Equal(subargs.Arguments[2].Start, 23)
	is.Equal(subargs.Arguments[2].End, 39)

	// appendstring
	newargs := noOkArgs(args.AppendString("test"))
	is.Equal(newargs.Strings(), []string{"this", "is", "a", "test", "string",
		"quoted string", "and an escape", "test"})
}

// A test that covers the boolean returned by most Arguments methods
// includes testing in states that it shouldn't reasonably be in
// (such as states with the cursor < 0 and cursor > length)
func TestArgumentsOkBoolean(t *testing.T) {
	is := is.New(t)

	const input = `test string one`

	args, ok := arguments.Parse(input)
	is.Equal(ok, true)

	is.Equal(args.Strings(), []string{"test", "string", "one"})

	// Test all possible cases for args.Current
	{
		_, ok = args.Current()
		is.Equal(ok, true)

		args.Cursor = -1
		_, ok = args.Current()
		is.Equal(ok, false)

		args.Cursor = len(args.Arguments)
		_, ok = args.Current()
		is.Equal(ok, false)
	}

	args.Cursor = 0

	// Test all possible cases for args.Previous
	{
		_, ok = args.Previous()
		is.Equal(ok, false)

		args.Cursor = 1
		_, ok = args.Previous()
		is.Equal(ok, true)

		args.Cursor = len(args.Arguments)
		_, ok = args.Previous()
		is.Equal(ok, true)

		args.Cursor = len(args.Arguments) + 1
		_, ok = args.Previous()
		is.Equal(ok, true)
	}

	args.Cursor = 0

	// Test all possible cases for args.Next
	{
		_, ok = args.Next()
		is.Equal(ok, true)

		args.Cursor = len(args.Arguments) - 1
		_, ok = args.Next()
		is.Equal(ok, false)

		args.Cursor = len(args.Arguments)
		_, ok = args.Next()
		is.Equal(ok, false)

		args.Cursor = -1
		a, ok := args.Next()
		is.Equal(ok, true)
		is.Equal(a.Content, "test")
	}

	args.Cursor = 0

	// Test all possible cases for args.Seek
	{
		ok = args.Seek(0)
		is.Equal(ok, true)

		ok = args.Seek(len(args.Arguments))
		is.Equal(ok, false)

		ok = args.Seek(-1)
		is.Equal(ok, false)
	}

	args.Cursor = 0

	// Test all possible cases for args.SeekBy
	{
		ok = args.SeekBy(0)
		is.Equal(ok, true)

		ok = args.SeekBy(len(args.Arguments))
		is.Equal(ok, false)

		args.Cursor = len(args.Arguments)

		ok = args.SeekBy(-1)
		is.Equal(ok, true)

		args.Reset()
		ok = args.SeekBy(1)
		is.Equal(ok, true)

		ok = args.SeekBy(-2)
		is.Equal(ok, false)
	}

	args.Cursor = 0

	// Test all possible cases for args.Peek
	{
		_, ok = args.Peek()
		is.Equal(ok, true)

		args.Cursor = len(args.Arguments)
		_, ok = args.Peek()
		is.Equal(ok, false)

		args.Cursor = -1
		_, ok = args.Peek()
		is.Equal(ok, true)

		args.Cursor = -2
		_, ok = args.Peek()
		is.Equal(ok, false)
	}

	args.Cursor = 0

	// Test all possible cases for args.PeekBy
	{
		_, ok = args.PeekBy(0)
		is.Equal(ok, true)

		args.Cursor = len(args.Arguments)
		_, ok = args.PeekBy(0)
		is.Equal(ok, false)

		args.Cursor = -1
		_, ok = args.PeekBy(0)
		is.Equal(ok, false)

		_, ok = args.PeekBy(1)
		is.Equal(ok, true)

		args.Cursor = 1
		_, ok = args.PeekBy(-1)
		is.Equal(ok, true)

		_, ok = args.PeekBy(1)
		is.Equal(ok, true)

		_, ok = args.PeekBy(2)
		is.Equal(ok, false)

		_, ok = args.PeekBy(-2)
		is.Equal(ok, false)
	}

	args.Cursor = 0

	// Test all possible cases for args.Raw
	{
		_, ok = args.Raw(0)
		is.Equal(ok, true)

		_, ok = args.Raw(len(args.Arguments))
		is.Equal(ok, false)

		_, ok = args.Raw(-1)
		is.Equal(ok, false)
	}

	args.Cursor = 0

	// Test all possible cases for args.RawBy
	{
		_, ok = args.RawBy(0)
		is.Equal(ok, true)

		_, ok = args.RawBy(len(args.Arguments))
		is.Equal(ok, false)

		_, ok = args.RawBy(-1)
		is.Equal(ok, false)

		args.Cursor = 1

		_, ok = args.RawBy(-1)
		is.Equal(ok, true)

		_, ok = args.RawBy(1)
		is.Equal(ok, true)

		_, ok = args.RawBy(2)
		is.Equal(ok, false)

		_, ok = args.RawBy(-2)
		is.Equal(ok, false)
	}

	args.Cursor = 0

	// Test all possible cases for args.RawRange
	{
		_, ok = args.RawRange(0, 0)
		is.Equal(ok, false)

		_, ok = args.RawRange(0, 1)
		is.Equal(ok, true)

		_, ok = args.RawRange(0, len(args.Arguments))
		is.Equal(ok, true)

		_, ok = args.RawRange(-1, 1)
		is.Equal(ok, false)

		_, ok = args.RawRange(0, len(args.Arguments)+1)
		is.Equal(ok, false)

		_, ok = args.RawRange(2, 0)
		is.Equal(ok, false)
	}

	args.Cursor = 0

	// Test all possible cases for args.Range
	{
		_, ok = args.Range(0, 0)
		is.Equal(ok, false)

		_, ok = args.Range(0, 1)
		is.Equal(ok, true)

		_, ok = args.Range(0, len(args.Arguments))
		is.Equal(ok, true)

		_, ok = args.Range(-1, 1)
		is.Equal(ok, false)

		_, ok = args.Range(0, len(args.Arguments)+1)
		is.Equal(ok, false)

		_, ok = args.Range(2, 0)
		is.Equal(ok, false)
	}

	args.Cursor = 0

	// Test all possible cases for args.AppendString
	{
		_, ok = args.AppendString(`test string two`)
		is.Equal(ok, true)

		_, ok = args.AppendString(``)
		is.Equal(ok, false)
	}
}
